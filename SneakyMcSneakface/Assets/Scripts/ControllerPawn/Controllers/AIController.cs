﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inherit from Controller
public class AIController : Controller { // class that is a subclass of the Superclass Controller

	float lastMovedTime; // Creates a float that is not public to the inspector

	public float timeToMove; // creates a public float called timeToMove for the AI that will cycle every second before making a move
    AISense senses; // Uses the AISense script and calls it senses
    public GameObject Target; // creates a componenet in the inspector called Target so we can insert the player to target
    public int PlayerDirection() // Function called PlayerDirection
    {
        var angleBetweenPlayerAndEnemy = Vector3.SignedAngle(transform.right, Target.transform.position - transform.position, Vector3.forward); // This will guide the ai to the player by rotation
        if (angleBetweenPlayerAndEnemy > 0) // beginning if statement if the player is less than 0
        {
            return -1;// if its not then the number is -1 which is the idle state
        }
        else if (angleBetweenPlayerAndEnemy < 0)
        {
            return 1; //  if its greater than the player will chase
        }
        return 0;
    }

    // Use this for initialization
    public override void Start()
	{
		// Call the Controller base class Start
		base.Start(); // Inherits the start function from Controller which is linked to Pawn
		lastMovedTime = Time.time; // get time information and sets it to lastMovedTime
        senses = GetComponent<AISense>(); // get the componenet for AISense script and attaches it to senses so we dont have to
    }

	// Update is called once per frame
	void Update ()
	{
        float movement = 0; // this is the chase state to move forward
        // See if it's time to move the pawn
        if ((Time.time - lastMovedTime) > timeToMove)
		{
			// the AI controls the pawn using random movements
			lastMovedTime = Time.time;
            // causes AI to turn towards player
            if (senses.SeeingPlayer())
            {
                movement = 0; // if the ai sees the player, it will move forward towards the player
            }
            else if (senses.HearingPlayer())
            {
                if (PlayerDirection() == -1) // if the ai doesnt hear the player, it will stop moving and stand idle
                {
                    movement = 2; // if the Ai sees the player it will change movement
                }
                else
                {
                    movement = 3; // And it will change rotation
                }
            }
            else
            {
                movement = -1; // if Ai loses hearing, the ai will stop and idle again
            }
            // Move the pawn
            if (movement == 0) // This is the number for moving forward
			{
				pawn.MoveForward();
			}
			else if (movement == 1) // this is the number for moving backwards
			{
				pawn.MoveBackward();
			}
			else if (movement == 2) // this is the number for turning left
			{
				pawn.RotateLeft();
			}
			else if (movement == 3) // this is the number for turning right
			{
				pawn.RotateRight();
			}
		}
	}

   
}