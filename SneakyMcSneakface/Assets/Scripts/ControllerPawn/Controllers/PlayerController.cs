﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inherit from Controller
public class PlayerController : Controller
{
	// Use this for initialization
	public override void Start()
	{
		// Call the Controller base class Start
		base.Start();
	}

	// Update is called once per frame
	void Update()
	{
		// The player controller gets input from the keyboard and then moves the pawn.
		if (Input.GetKey(KeyCode.W)) // This will move our player when we press the W key and will initiate the MoveForward function in the superclass Pawn
		{
			pawn.MoveForward();
		}
		if (Input.GetKey(KeyCode.S))// This will move our player when we press the S key and will initiate the MoveBackward function in the superclass Pawn
        {
			pawn.MoveBackward();
		}

		// Only the person will rotate, but always handle the input
		if (Input.GetKey(KeyCode.A))// This will rotate our player when we press the A key and will initiate the RotateLeft function in the superclass Pawn
        {
			pawn.RotateLeft();
		}

		// Only the person pawn will rotate, but always handle the input
		if (Input.GetKey(KeyCode.D))// This will rotate our player when we press the D key and will initiate the RotateRight function in the superclass Pawn
        {
			pawn.RotateRight();

		}

		// All pawns can go home
		if (Input.GetKey(KeyCode.H))// This will send out player back to the orignal spot from the game initiation when we press the H key
		{
			pawn.GoHome();
		}
	}
}