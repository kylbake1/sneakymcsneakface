﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPawn : Pawn { //  This is a subclass of the super class Pawn

	// Use this for initialization
	public override void Start()
	{
		// Call the base Pawn class Start
		base.Start();
	}

	// Update is called once per frame
	public override void Update()
	{
		// Call the base Pawn class Update
		base.Update();
	}

	public override void MoveForward()
	{
		tf.transform.Translate(Time.deltaTime * speed, 0, 0); //  This will control the AI Pawns movement when MoveForward function is initiated
	}

	public override void MoveBackward()
	{
		tf.transform.Translate(-Time.deltaTime * speed, 0, 0); //  This will control the AI Pawns movement when MoveBackward function is initiated
    }

	public override void RotateLeft()
	{
		tf.transform.Rotate(0, 0, Time.deltaTime * rotationSpeed); //  This will control the AI Pawns rotation in the positive direction when the RotateLeft function is initiated
    }

	public override void RotateRight()
	{
		tf.transform.Rotate(0, 0, -Time.deltaTime * rotationSpeed);//  This will control the AI Pawns rotation in the negative direction when the Rotateright function is initiated
    }
}
