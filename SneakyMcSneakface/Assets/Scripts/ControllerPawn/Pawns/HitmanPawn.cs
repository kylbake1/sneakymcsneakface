﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitmanPawn : Pawn // This class is a subclass fo the superclass Pawn
{
	// Use this for initialization
	public override void Start()
	{
		// Call the base Pawn class Start
		base.Start(); // This will use the SuperClass Pawn's start function
	}

	// Update is called once per frame
	public override void Update()
	{
		// Call the base Pawn class Update
		base.Update(); // This will use the update function in the pawn SuperClass
	}

	public override void MoveForward() // This will override the original MoveForward function so this pawn can have it's own move commands
	{
		tf.transform.Translate(Time.deltaTime * speed, 0, 0);
	}

	public override void MoveBackward() // This will override the original BackForward function so this pawn can have it's own move commands
    {
		tf.transform.Translate(-Time.deltaTime * speed, 0, 0);
	}

	public override void RotateLeft() // This will override the original RotateLeft function so this pawn can have it's own move commands
    {
		tf.transform.Rotate(0, 0, Time.deltaTime * rotationSpeed);
	}

	public override void RotateRight()// This will override the original RotateRight function so this pawn can have it's own move commands
    {
		tf.transform.Rotate(0, 0, -Time.deltaTime * rotationSpeed);
	}

	public override void GoHome()// This will give the pawn it's own home postition than the other pawns
	{
		tf.transform.position = homePosition;
	}

}