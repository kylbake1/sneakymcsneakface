﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; // gives access to loading scenes


public class HealthDisplay : MonoBehaviour
{
    public static GameManager instance;

    
    public Text healthText; // creates a public text called healthText that will appear in the inspector for modifying

    
    // Update is called once per frame
    void Update()
    {
        healthText.text = "Health : " + GameManager.instance.health; // this will update the UI for the current number of lives

        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameManager.instance.health--; // Will deduct the number of lives by 1
        }
    }
}
